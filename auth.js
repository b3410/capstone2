//[SECTION] Dependencies and Modules
	const jwt = require('jsonwebtoken');
	const dotenv = require('dotenv');

//[SECTION] Environment Variable Setup
	dotenv.config();
	let secret = process.env.SECRET;

//[SECTION] Functionalities
		module.exports.createAccessToken = (authUser) => {
		
			let userData = {
				id: authUser._id,
				email: authUser.email,
				isAdmin: authUser.isAdmin
			}

			return jwt.sign(userData, secret, {});
		};


		module.exports.verify = (req, res, next) => {
			let token = req.headers.authorization;
			if (typeof token !== 'undefined') {

				token = token.slice(7, token.length);
				return jwt.verify(token, secret, (err, payload) => {
					(err) ? res.send('Failed Authorization')
					: next()
				})

			} else {
				return res.send({auth: "Authorization Failed, check token!"})
			}
		};

		module.exports.decode = (accessToken) => {
			if (typeof accessToken !== 'undefined') {
				accessToken = accessToken.slice(7, accessToken.length);
				return jwt.verify(accessToken, secret, (error, verified) => {
					if (error) {
						return null;
					} else {
						return jwt.decode(accessToken, {complete:true}).payload;
					}
				})
			} else {
				return null;
			}
		}


