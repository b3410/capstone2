
//  [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');

//  [SECTION] Routing Components
	const route = exp.Router()

//  [SECTION]-[POST] Route (ad)
	route.post('/', auth.verify, (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			product: req.body,
		};
		if (isAdmin) {
		controller.addProduct(data).then(outcome => {
			res.send(outcome);
		});

		} else {
			res.send('User unauthorized to proceed')
		}
	});

//  [SECTION]-[GET] Route
	route.get('/all', auth.verify, (req,res) => {
		let token = req.headers.authorization
		let payload = auth.decode(token); 
		let isAdmin = payload.isAdmin;
		(isAdmin) ?
		controller.getAllProduct().then(result => res.send(result))
		:
			res.send('Unauthorized User')
	});


	route.get('/', (req,res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	
	route.get('/:id', (req,res) => {
		let data = req.params.id
		controller.getProduct(data).then(result => {
			res.send(result);
		});
	});

//  [SECTION]-[PUT] Route
	//Update Product Details
	route.put('/:productId', auth.verify,(req, res) => {

		let params = req.params;
		let body = req.body;
		if (!auth.decode(req.headers.authorization).isAdmin) {
			res.send('user unauthorized')
		} else {
			controller.updateProduct(params, body).then(outcome => {
				res.send(outcome);
			});
		};

	});

	//Product Archive
	route.put('/:productId/archive', auth.verify, (req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? 
				controller.archiveProduct(params).then(outcome => {
					res.send(outcome);
			})
			:
				res.send('Unauthorized User');

	});


	//Product reactivation
	route.put('/:productId/reactivation', auth.verify, (req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? 
				controller.unarchiveProduct(params).then(outcome => {
					res.send(outcome);
			})
			:
				res.send('Unauthorized User');

	});

//  [SECTION]-[DELETE] Route
	route.delete('/:productId', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.productId;
		isAdmin ?
		controller.deleteProduct(id).then(outcome => {
			res.send(outcome);
		})
		: res.send('Unauthorized User')
	});



//  [SECTION] Export Route System
	module.exports = route;