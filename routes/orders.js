//  [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');

//  [SECTION] Routing Components
	const route = exp.Router();


//[SECTION] Create Order
	route.post('/orderId', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let userId = payload.id;
	let isAdmin = payload.isAdmin;
	let producto = req.body.productId;
	let qty = req.body.quantity;
	let info = {
		userId: userId,
		productId: producto,
		quantity: qty
	};
	if (!isAdmin) {
		controller.createOrder(info).then(result => {
			res.send(result);
		})
	} else {
		res.send("Admins are not allowed to make an order")
	}
});



//[SECTION] Retrieve All Orders
//Get All Orders (Admin)
	route.get('/all', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		if (isAdmin) {
			controller.getAllOrders().then(result => {
				res.send(result);
			});
		} else {
			res.send('Regular Users are not allowed to access this page!')
		}
		
	});

	

	// Retrieve Authenticated User's Orders
	route.get('/user-order', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		let userId = payload.id;
		isAdmin ? res.send(`User Unauthorized to Proceed!`)
		: controller.getUserOrder(userId).then(outcome => res.send(outcome))
	});


//  [SECTION] Export Route System
	module.exports = route;
