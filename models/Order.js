// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose'); 
	const Product = require('./Products');
	const User = require('./User');

// [SECTION] Schema
	const orderSchema = new mongoose.Schema({

		totalAmount:{
			type: Number,
			required: [true, "Total Amount is Required!"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
	
		userId : {
			type: String,
			required: [true, "User Id is Required!"]
		},
		productId: {
			type: String,
			required: [true, "Product Id is Required!"]
		}
	
	});


// [SECTION] Model
   module.exports = mongoose.model("Order", orderSchema);