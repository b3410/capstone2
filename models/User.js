//[SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

//[SECTION] Schema
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, 'First Name is Required!']
		},
		lastName: {
			type: String,
			required: [true, 'Last Name is Required!']
		},
		middleName: {
			type: String,
			required: [true, 'Middle Name is Required!']
		},
		email: {
		    type: String,
		    required: [true, 'Email is Required!']
		},
		password: {
		    type: String,
		    required: [true, 'Password is Required!']

		},
		mobileNumber: {
			type: String,
			required: [true, 'Mobile Number is Required!']
		},

		address: {
			type: String,
			required: [true, 'Home Address is Required!']
		},

		isAdmin: {
			type: Boolean, 
			default: false 
		},
		orders: [{
			productId: {
				type: String,
				required: [true, "This field is Required!"]
			}
		}]
	}); 

//[SECTION] Model
	module.exports = mongoose.model('User', userSchema);
