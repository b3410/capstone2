//[SECTION] Dependencies and Modules
  const User = require('../models/User');
  const Product = require('../models/Products');
  const bcrypt = require('bcrypt'); 
  const auth = require('../auth');

//[SECTION] Functionalities [Create]

     module.exports.registerUser = (data) => {
         let fName = data.firstName;
         let lName = data.lastName;
         let mName = data.middleName;
         let email = data.email;
         let passW = data.password;
         let mobileNo = data.mobileNumber;
         let address = data.address

         let newUser = new User({
            firstName: fName,
            lastName: lName,
            middleName: mName,
            email: email,
            password: bcrypt.hashSync(passW, 10),
            mobileNumber: mobileNo,
            address: address
         });

         return User.findOne({email:email}).then(found => {
            if (found) {
               return `Email already exists! Please try another email.`
            } else {
               return newUser.save().then((user, err) => {
                  if (user) {
                     return user;
                  } else {
                     return false;
                  }
               });
            }
         });
      };



 
//[SECTION] Functionalities [Retrieve]

   //Single User/ Order
      module.exports.getProfile = (id) => {
         return User.findById(id).then(result => {
            return result;
         });
      };

   //login
   module.exports.loginUser = (reqBody) => {
         let email = reqBody.email;
         let pass = reqBody.password;
         return User.findOne({email: email}).then(result => {
            if (result === null) {
               return false;
            } else {
            let passW = result.password;
               const isMatched = bcrypt.compareSync(pass, passW);
      
               if (isMatched) {
                  console.log(result);
                  let data = result.toObject();
                  console.log(data)
                  return {access: auth.createAccessToken(data)};
               } else {
                  return false;
               }
            };
         });
      };


//[SECTION] Functionalities [Update]
   //Set User as Admin
      module.exports.setAsAdmin = (userId) => {
         
         let updates = {
            isAdmin: true
         }
         return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
            if (admin) {
               return true;
            } else {
               return 'Failed to update';
            }
         });
      };

   //Set User as Non-Admin
      module.exports.setAsNonAdmin = (userId) => {

         let updates = {
            isAdmin: false
         }
         return User.findByIdAndUpdate(userId, updates).then((user, err) => {
            if (user) {
               return true;
            } else {
               return 'Failed to update'
            }
         });
      };


   // Change Password
   module.exports.updatePassword = (reqBody) => {
      let uEmail = reqBody.email;
      let uPass = reqBody.password;
      let nPass = reqBody.newpassword;

      return User.findOne({email: uEmail}).then(result => {
         if (result === null) {
            return `Email does not exist!`;
         } else {
         let passW = result.password;
         let isMatched = bcrypt.compareSync(uPass, passW);
            if (isMatched) {
               let dataNiUser = result.toObject();
               let updates = {
                  password: bcrypt.hashSync(nPass, 10)
                  }
               return User.findByIdAndUpdate(dataNiUser, updates).then((user, err) => {
                  if (user) {
                     return `Successfully updated the password!`;
                  } else {
                     return `Unable to update user information.`;
                  };
               });
            } else {
               return `Passwords does not match. Check credentials.`
            };
         };
      });
   };


//[SECTION] Functionalities [Delete]