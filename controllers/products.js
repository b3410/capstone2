// [SECTION] Dependencies and Modules
	const Product = require('../models/Products');

// [SECTION] Functionality [Create]
	module.exports.addProduct = (info) => {
		let product = info.product
		let pName= product.name;
		let pDesc= product.description;
		let pPrice= product.price;

		let newProduct = new Product({
			name: pName,
			description: pDesc,
			price: pPrice
		});

		return newProduct.save().then((savedProduct, error) => {
				if (savedProduct) {
					return savedProduct; 
				} else {
					return false;
				}	
		});
		
	};

// [SECTION] Functionality [Retrieve]

	module.exports.getAllProduct = () => {
		return Product.find({}).then(result => {
			return result;
		});
	};

	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		});
	};

	module.exports.getProduct = (id) => {
		return Product.findById(id).then(result => {
			return result;
		});
	};

// [SECTION] Functionality [Update]
	//Update Product Details 

	module.exports.updateProduct= (product, details) => {
		let pName = details.name;
		let pDesc= details.description;
		let pPrice = details.price;

		let updatedProduct ={
			name: pName ,
			description: pDesc ,
			price: pPrice
		};

		let id = product.productId;

		return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
			if (productUpdated) {
				return productUpdated;
			} else {
				return 'Failed to update product';
			};
		});

	};


	//Archive Product
	module.exports.archiveProduct = (product) => {
		let id = product.productId
		let updates = {
			isActive: false
		};
		return Product.findByIdAndUpdate(id, updates).then((archived, err) => {
			if (archived) {
				return archived;
			} else {
				return 'Failed to update status'
			}
		});
	};

	//Product Reactivation
	module.exports.unarchiveProduct = (product) => {
		let id = product.productId
		let updates = {
			isActive: true
		};
		return Product.findByIdAndUpdate(id, updates).then((active, err) => {
			if (active) {
				return active;
			} else {
				return 'Failed to update status'
			}
		});
	};


// [SECTION] Functionality [Delete]
	//Delete Product
		module.exports.deleteProduct = (productId) => {
			return Product.findById(productId).then(product => {
				if (product === null) {
					return 'No product found'
				} else {
					return product.remove().then((removedProduct, err) => {
						if (err) {
							return 'Failed to remove product'
						} else {
							return 'Successfully Deleted'
						};
					});
				};
			});
		};

		