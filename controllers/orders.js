/// [SECTION] Dependencies and Modules
	const Order = require('../models/Order');
	const Product = require('../models/Products');
	const User = require('../models/User');
	const auth = require('../auth');

// [SECTION] Funtionalities [Create]
	// Create Order
	module.exports.createOrder = async (info) => {

		let producto = info.productId;
		let qty = info.quantity;
		let id = info.userId;
		let orders = info.orders;

		let total = await Product.findById(producto).then(result => result.price*qty);
		console.log(total);
		let userData = await User.findById(id).then(outcome => {
			outcome.orders.push({productId: producto});
			return outcome.save().then((save, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		});

	
		let newOrder = new Order({
			productId: producto,
			quantity: qty,
			totalAmount: total,
			userId: id
		});

		return newOrder.save().then((save, err) => {
			if (err) {
				return `There was an error in processing your order to the database.`;
			} else {
				return save;
			}
		});
	};

// [SECTION] Functionalities [GET]
	// Retrieve ALL Orders
	module.exports.getAllOrders = () => {
		return Order.find({}).then(outcome => {
			return outcome;
		});
	};

  // Retrieve Authenticated User's Orders
	module.exports.getUserOrder = (id) => {
		return Order.find({userId: id}).then(order => {
			return order;
		});
	};
